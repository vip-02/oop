const obj = {
  // key / property
  a: 1,
  b: "a",
  c: true,
  d: [1, 2, 3],
  h: null,
  i: undefined,

  // method

  // 1
  e: function () {},
  // 2
  f() {},
  // 3
  g: () => {},
};

class Square {
  x = 0;

  constructor(sisi) {
    this.x = sisi;
  }

  print() {
    console.log(this.x);
  }
}

const kotak1 = new Square(6);
const kotak2 = new Square(9);

// buat class Animal
// properti nama = "Anjing", "Kucing"
// metode suara = "woof", "meow"

class Animal {
  nama;
  _suara;

  constructor(nama, suara) {
    this.nama = nama;
    this._suara = suara;
  }

  suara() {
    console.log(this._suara);
  }
}

const anjing = new Animal("Anjing", "woof");
anjing.suara()

const kucing = new Animal("Kucing", "meow");
kucing.suara()
// anjing.nama // "anjing"
// anjing.suara // "woof"
